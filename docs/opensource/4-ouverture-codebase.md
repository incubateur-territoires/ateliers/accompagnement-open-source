---
title: Ouverture du reste de la base de code
---

_Atelier collectif, durée 3h_

L'objectif est d'ouvrir un temps dédié à l'exploration de cette éventualité, afin de réfléchir ensemble aux réponses aux diverses questions et problématiques vis à vis de cette ouverture.

A rédiger...
