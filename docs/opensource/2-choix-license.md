---
title: Choix de license
---

_Atelier collectif, durée 1h_

L'idée est que l'équipe puisse presenter ce qu'ils ont en tête, qu'on puisse en parler pour éviter d'avoir des angles morts, que l'accompagnateur puisse vous apporter quelques billes sur le sujet et qu'on puisse décider ensemble d'une licence qui soit adapté à la situation du projet.

**Déroulé:**

1. **Intro:** tour de table des attentes vis à vis de l'atelier
2. Qu’est-ce que vous savez déjà des licenses opensource ?
3. Est-ce que vous avez des questions sur le sujet avant d'attaquer ?
4. Les différents types de license, vos contraintes & envies.
   - Copyleft vs permissivité
   - Strong copyleft vs weak copyleft
   - L'importance d'utiliser une license standard
5. Définition d'une license pour vos besoins
   - Recommandation de l'accompagnateur
   - Idées et réaction des porteur·euses de projet
   - Prise de décision par l'équipe
6. **Conclusion** tour de table de ressenti et feedback sur l'atelier
