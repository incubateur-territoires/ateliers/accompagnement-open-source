---
title: Présentations
---

Un premier appel est effectué pour faire connaissance et essayer de comprendre l'état d'esprit des porteurs·euses de projets vis à vis de l'opensource (sont-iels enthousiaste, se sentent-iels obligé·es, sont-iels inquiet·es, ...)

L'objectif de cet appel est d'avoir un premier aperçu des connaissances et a priori que l'équipe a sur le sujet.
